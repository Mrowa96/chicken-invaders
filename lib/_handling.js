function _mouse(){
    var that = {};
    
    that.handle = function(e){
        var event = e || window.event;
        gl.body[0].style.cursor = "none"
        document.addEventListener("mousemove",function(event){
            gl.player.x = event.clientX;
            gl.player.y = event.clientY;
        },false)
    }

    return that;
}

function _keyboard(e){
    var t = {},
        e = e || window.event;
        
    t.gContext = gl.gameCanvasContext;
    t.gCanvas = gl.gameCanvas;
    t.time = 0;
    
    window.addEventListener("keydown",function(e){
        gl.key['k_'+e.keyCode] = true;
    },false);

    window.addEventListener("keyup",function(e){
        gl.key['k_'+e.keyCode] = false;
        if(gl.player){
            gl.player.isLeft = false;
            gl.player.isRight = false;
            gl.player.isShoot = false;
        }
    },false);
        
    t.handle = function(){
        if(gl.player && !gl.player.destroyBool){
            if(!gl.level.stopped){
                if (gl.key['k_37'] && gl.player.x > 0){
                    if(gl.player.x-gl.player.vx >=0 ){
                        gl.player.x -= gl.player.vx;
                    }
                    else{
                        gl.player.x = 0;
                    }
                    gl.player.isLeft = true;
                }
                if (gl.key['k_39'] && gl.player.x < settings.width-gl['spaceship'].width){
                    if(gl.player.x < settings.width-gl['spaceship'].width){
                        gl.player.x += gl.player.vx;
                    }
                    else{
                        gl.player.x = settings.width;
                    }
                    gl.player.isRight = true;
                }
                if (gl.key['k_38'] && gl.player.y > 0){
                    if(gl.player.y-gl.player.vy >=0 ){
                        gl.player.y -= gl.player.vy;
                    }
                    else{
                        gl.player.y = 0;
                    }
                }
                if (gl.key['k_40'] && gl.player.y < settings.height-gl['spaceship'].height){
                    if(gl.player.y < settings.height-gl['spaceship'].height){
                        gl.player.y += gl.player.vy;
                    }
                    else{
                        gl.player.y = settings.height;
                    }
                }

                if(gl.key['k_32']){
                    if(new Date().getTime() - t.time > 200){
                        new _bullet();
                        new _audio().shoot();
                        t.time = new Date().getTime();
                        gl.player.isShoot = true;
                    }
                }
            }

            if(gl.key['k_27']){
                if(new Date().getTime() - t.time > 200){
                    if(gl.getDom("statScreen").style.display == "block"){
                        gl.getDom("statScreen").style.display = "none"
                        gl.level.stopped = false;
                    }
                    else{
                        var time = new Date().getTime()-gl.gameStartVal;
                        if(time/1000 < 60){
                            gl.game_time.innerHTML = time/1000+"s";
                        }
                        else{
                            minutes = parseInt(time/1000/60);
                            secundes = parseFloat((time/1000)-(minutes*60),2)
                            gl.game_time.innerHTML = minutes+"m"+" "+secundes+"s";
                        }
                        
                        gl.game_killed.innerHTML = gl.gameKilledVal;
                        gl.game_died.innerHTML = gl.gameDiedVal;
                        gl.getDom("statScreen").style.display = "block";
                        gl.level.stopped = true;

                    }
                    t.time = new Date().getTime();
                    
                }
                
            }
        }
    }
    return t;
}
